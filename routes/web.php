<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Logistik
Route::get('/logistik', 'LogistikController@index')->name('logistik.index');
Route::get('/logistik/edit/{id}', 'LogistikController@edit')->name('logistik.edit');
Route::get('/logistik/update/{id}', 'LogistikController@update')->name('logistik.update');
Route::get('/logistik/getDataLogistik', 'LogistikController@getDataLogistik')->name('logistik.getDataLogistik');
Route::post('/logistik/create', 'LogistikController@create')->name('logistik.create');
Route::post('/logistik/store', 'LogistikController@store')->name('logistik.store');


// API