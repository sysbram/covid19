<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class logistik extends Model
{
    protected $table = 'logistik';

    protected $fillaable = [
        'id_logistik',
        'id_user',
        'alamat_isolasi',
        'gejala',
        'tgl_swab',
        'file_swab',
    ];
}
