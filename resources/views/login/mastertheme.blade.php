<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield("title","Login")</title>
    <meta name="robots" content="noindex">
    
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">
    
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/vendor/simplebar.min.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.rtl.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-material-icons.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-material-icons.rtl.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-fontawesome-free.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-fontawesome-free.rtl.css" rel="stylesheet">
    @yield('css')
</head>

<body class="layout-login">

<div class="layout-login__overlay"></div>
<div class="layout-login__form" style="background-color: #0c2f4f;" data-simplebar>
    <div class="d-flex justify-content-center mt-0 mb-5 navbar-light">
        <a href="index.html" class="navbar-brand" style="min-width: 0">
            <img class="navbar-brand-icon" src="{{ asset('assets/theme/dist') }}/assets/images/logo.png" width="150" alt="Stack" style="padding-left: 25px;">
        </a>
    </div>

    <h4 class="m-0 text-center text-light">Login</h4>
    <br>
    <br>

    @yield('content')

</div>


<!-- jQuery -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/popper.min.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/bootstrap.min.js"></script>
<!-- Simplebar -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/simplebar.min.js"></script>
<!-- DOM Factory -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/dom-factory.js"></script>
<!-- MDK -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/material-design-kit.js"></script>
<!-- App -->
<script src="{{ asset('assets/theme/dist') }}/assets/js/toggle-check-all.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/check-selected-row.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/dropdown.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/sidebar-mini.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/app.js"></script>
<!-- App Settings (safe to remove) -->
<script src="{{ asset('assets/theme/dist') }}/assets/js/app-settings.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133433427-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-133433427-1');
</script>

<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '327167911228268');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=327167911228268&ev=PageView&noscript=1" /></noscript>
@yield('js')
</body>

</html>
